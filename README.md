# Python Scripts
This project contains quick dirty scripts for doing whatever I want. 

## Note
At the beginning of each script we define `PYTHON=python`. It's expected that you are on at least
version 3.

## Config
Ensure you have properly configured the correct configs under `config.cfg`

## run.sh
This script will run whatever it is we want aww yiss

**Usage**:
```bash
$ ./run.sh 
```

## SSH Tunneling
Some scripts may involve tunneling to access db data:

```bash
ssh -fNL 2345:<db_host>.us-east-1.rds.amazonaws.com:5432 -i <path_to_rsa> <user>@<ip>
```
__Note__: The `2345` port corresponds to the `db.port` config value
__Note__: The `ip` corresponds to the EC2 instance that can connect to the DB
import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name='kewley_python_scripts',
    version='0.0.2',
    author='Mark Kewley',
    python_requires='>= 3.6',
    author_email='markkewley1990@gmail.com',
    description='Scripts for running whatever I need for testing purposes',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/kewley-public/python_scripts',
    packages=setuptools.find_packages(),
    install_requires=[
        'boto3',
        'pg8000'
    ],
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ]
)

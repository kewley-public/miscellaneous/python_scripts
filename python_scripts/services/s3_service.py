import json
from configparser import ConfigParser

import boto3


class S3Service:
    def __init__(self, config=None):
        if config is None:
            config = ConfigParser()
            config.read('config.cfg')
        self.optimizer_input_bucket_name = config['s3']['input_bucket_name']
        self.s3_region = config['s3']['region']
        self.s3 = boto3.resource('s3', region_name=self.s3_region)

    def save_input(self, key, update_json):
        try:
            obj = self.s3.Object(self.optimizer_input_bucket_name, key)
            obj.put(Body=update_json)
        except Exception as e:
            raise e

    def retrieve_optimizer_input_json_as_string(self, key):
        try:
            obj = self.s3.Object(self.optimizer_input_bucket_name, key)
            return obj.get()['Body'].read().decode('utf-8')
        except Exception as e:
            raise e

    def retrieve_optimizer_s3_keys(self, filter_key=None):
        try:
            bucket = self.s3.Bucket(self.optimizer_input_bucket_name)
            objects = []
            for obj in bucket.objects.all():
                if filter_key is not None:
                    if filter_key in obj.key:
                        objects.append(str(obj.key))
                else:
                    objects.append(str(obj.key))
            return objects
        except Exception as e:
            raise e

    def fetch_jobs_by_job_plan(self, date, job_plan_number):
        try:
            keys = self.retrieve_optimizer_s3_keys(date)
            jobs = []
            for key in keys:
                optimizer_input = self.retrieve_optimizer_input_json_as_string(key)
                input_as_json = json.loads(optimizer_input)
                if 'jobs' in input_as_json:
                    filtered = list(
                        filter(lambda x: x['job_plan_number'] == job_plan_number if 'job_plan_number' in x else False,
                               input_as_json['jobs']))
                    jobs = jobs + filtered
            return jobs
        except Exception as e:
            raise e

    def perform_update_on_dates(self, date_keys, update):
        updated_keys = []
        try:
            keys = self.retrieve_optimizer_s3_keys()
            keys_to_sync = []
            for date_key in date_keys:
                for key in keys:
                    if date_key in key:
                        keys_to_sync.append(key)

            count = 0
            for key in keys_to_sync:
                try:
                    optimizer_input = self.retrieve_optimizer_input_json_as_string(key)
                    if optimizer_input is not None:
                        input_as_json = json.loads(optimizer_input)
                        for update_key in update:
                            if update_key == 'job_updates':
                                if 'jobs' in input_as_json and len(input_as_json['jobs']) > 0:
                                    S3Service.update_jobs(input_as_json['jobs'], update[update_key])
                            else:
                                input_as_json[update_key] = update[update_key]
                        self.save_input(key, json.dumps(input_as_json))
                        updated_keys.append(key)
                        count += 1
                    else:
                        print('Unable to update key {}'.format(key))
                except Exception as e:
                    print('Unable to update key {}, error {}'.format(key, e))
            print('Updated {} input jsons out of {}'.format(count, len(keys_to_sync)))
        except Exception as e:
            print(e)
        return updated_keys

    @staticmethod
    def update_jobs(jobs, job_updates):
        for job in jobs:
            if 'fault_code_data' not in job:
                job['fault_code_data'] = []
            if 'job_plan_number' not in job:
                job['job_plan_number'] = None
            if 'job_id' in job:
                job_id = job['job_id']
                if job_id in job_updates:
                    for key in job_updates[job_id]:
                        job[key] = job_updates[job_id][key]

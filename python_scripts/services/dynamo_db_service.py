import datetime
from configparser import ConfigParser

import boto3
from boto3.dynamodb.conditions import Key


class DynamoDBException(Exception):
    pass


class DynamoDBService:
    def __init__(self, config=None):
        if config is None:
            config = ConfigParser()
            config.read('config.cfg')

        self.calculator_inputs_table_name = config['dynamoDB']['calculator_inputs_table_name']
        self.calculator_inputs_table_hash_key = 'runId'
        self.dynamo_db_region = config['dynamoDB']['region']
        self.dynamo_db = boto3.resource('dynamodb', region_name=self.dynamo_db_region)
        self.table = self.dynamo_db.Table(self.calculator_inputs_table_name)

    def create_items(self, items):
        mod_time = datetime.datetime.now().isoformat()
        mod_by = 'harambe'
        job_plan_numbers = set()
        with self.table.batch_writer() as batch:
            for item in items:
                if 'jobPlanNumber' in item:
                    job_plan_number = item['jobPlanNumber']
                    if job_plan_number in job_plan_numbers:
                        print('Skipping {} as it has already been created once'.format(job_plan_number))
                        continue
                    job_plan_numbers.add(job_plan_number)
                    item['lastModDate'] = str(mod_time)
                    item['lastModBy'] = mod_by
                    batch.put_item(
                        Item=item
                    )
                else:
                    print('Row skipped as jobPlanNumber was not present: [{}]'.format(item))
        return job_plan_numbers

    def delete_all_items(self):
        scan = self.table.scan(
            ProjectionExpression='#k, #s',
            ExpressionAttributeNames={
                '#k': 'jobPlanNumber',
                '#s': 'lastModDate'
            }
        )

        delete_count = 0
        with self.table.batch_writer() as batch:
            for each in scan['Items']:
                batch.delete_item(Key=each)
                delete_count += 1
        return delete_count

    def delete_by_ids(self, ids):
        items = []
        for key in ids:
            resp = self.fetch_all_items_by_id(key)
            if resp is not None and 'Items' in resp and len(resp['Items']) != 0:
                items = items + resp['Items']
        delete_count = 0
        with self.table.batch_writer() as batch:
            for item in items:
                batch.delete_item(Key={
                    'jobPlanNumber': item['jobPlanNumber'],
                    'lastModDate': item['lastModDate']
                })
                delete_count += 1
        return delete_count

    @staticmethod
    def group_by_job_plan_number(items):
        grouping = {}
        for item in items:
            key = item['jobPlanNumber']
            grouping[key] = item
        return grouping

    def fetch_all_items(self):
        response = self.table.scan()
        return self.group_by_job_plan_number(response['Items'])

    def fetch_items_by_id(self, key):
        return self.table.query(
            KeyConditionExpression=Key('jobPlanNumber').eq(key),
            ScanIndexForward=False,
            Limit=1
        )

    def fetch_all_items_by_id(self, key):
        return self.table.query(
            KeyConditionExpression=Key('jobPlanNumber').eq(key)
        )

    def update_items_by_keys(self, job_plan_number, last_mod_date, column_key, column_update):
        return self.table.update_item(
            Key={
                'jobPlanNumber': job_plan_number,
                'lastModDate': last_mod_date
            },
            UpdateExpression="set {} = :r".format(column_key),
            ExpressionAttributeValues={':r': column_update},
            ReturnValues="UPDATED_NEW"
        )

    def update_items_by_column_key(self, items, primary_key, column_key):
        update_count = 0
        for item in items:
            column_update = item[column_key]
            response = self.fetch_items_by_id(item[primary_key])
            for i in response['Items']:
                job_plan_number = i[primary_key]
                last_mod_date = i['lastModDate']
                self.update_items_by_keys(
                    job_plan_number,
                    last_mod_date,
                    column_key,
                    column_update
                )
                update_count += 1

        return update_count

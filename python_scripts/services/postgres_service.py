from configparser import ConfigParser

import pg8000


def parse_work_orders_result(result):
    updates = {}
    for item in result:
        (item_id, job_plan_number) = item
        updates[item_id] = {
            'job_plan_number': job_plan_number
        }
    return updates


class PostgresService:
    def __init__(self, config=None):
        if config is None:
            config = ConfigParser()
            config.read('config.cfg')

        postgres_config = config['postgres']

        self.host = postgres_config['host']
        self.port = int(postgres_config['port'])
        self.database = postgres_config['db']
        self.user = postgres_config['user']
        self.password = postgres_config['password']

    def connect(self):
        return pg8000.connect(
            host=self.host,
            port=self.port,
            database=self.database,
            user=self.user,
            password=self.password,
        )

    def search_for_all_cod_dates(self):
        db = self.connect()
        try:
            cur = db.cursor()
            cur.execute('SELECT * from sos.commercial_operation_date')
            return cur.fetchall()
        except Exception as e:
            db.rollback()
            print(e)
        finally:
            db.close()

    def fetch_all_work_order_updates(self):
        db = self.connect()
        try:
            cur = db.cursor()
            cur.execute(
                'SELECT id, job_plan_number FROM wos.work_order where'
                ' job_plan_number is NOT NULL and job_plan_number <> %s',
                [''])
            return parse_work_orders_result(cur.fetchall())
        except Exception as e:
            db.rollback()
            print(e)
        finally:
            db.close()
